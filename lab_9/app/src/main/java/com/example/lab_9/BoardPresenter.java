package com.example.lab_9;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

class BoardPresenter implements BoardListener {
    private BoardView boardView;
    public Board board;
    private List<CellClickListener> cellClickListeners = new ArrayList<>();
    public BoardPresenter(BoardView view) {
        this.boardView = view;
        board = new Board(this);
    }
    private void move(byte row, byte col) {
        board.move(row,col);
    }
    public void addCellClickListener(CellClickListener listener){
        cellClickListeners.add(listener);
    }
    @Override
    public void playedAt(byte player, byte row, byte col) {
        if (player == BoardListener.PLAYER_1){
            boardView.putSymbol(BoardView.PLAYER_1_SYMBOL, row, col);
        }else if(player == BoardListener.PLAYER_2){
            boardView.putSymbol(BoardView.PLAYER_2_SYMBOL, row, col);
        }
    }
    @Override
    public void gameEnded(byte winner) {
        switch (winner){
            case BoardListener.NO_ONE :
                boardView.gameEnded(BoardView.DRAW);
            case BoardListener.PLAYER_1 :
                boardView.gameEnded(BoardView.PLAYER_1_WINNER);
            case BoardListener.PLAYER_2 :
                boardView.gameEnded(BoardView.PLAYER_2_WINNER);
        }
    }
    static class CellClickListener implements View.OnClickListener{
        BoardPresenter presenter;
        byte row;
        byte col;
        public CellClickListener( BoardPresenter presenter, byte row, byte col){
            this.row = row;
            this.col = col;
            this.presenter = presenter;
        }
        @Override
        public void onClick(View view){
            Log.d("CellClickListener", "at" + row + ", " + col);
            presenter.move(row,col);
        }



        public boolean GameEnded(int r, int c,byte WhichPlayerValue){
            boolean gameisended = true;

            //row control

            for(int i = 0; i < 3; i++){
                if(presenter.board.board[r][i] != WhichPlayerValue){
                    gameisended = false;

                }

            }

            //column control
            if(gameisended){
                return gameisended;
            }
            gameisended= true;
            for(int i = 0; i < 3; i++){

                if(presenter.board.board[i][c] != WhichPlayerValue){
                    gameisended = false;

                }

            }

            //diagonal control

            if(gameisended){
                return gameisended;
            }
            gameisended= false;
            if((presenter.board.board[0][0] == WhichPlayerValue && presenter.board.board[1][1] == WhichPlayerValue&&
                    presenter.board.board[2][2] == WhichPlayerValue) || (presenter.board.board[0][2] == WhichPlayerValue &&
                    presenter.board.board[1][1] == WhichPlayerValue && presenter.board.board[2][0] == WhichPlayerValue)){
                gameisended = true;

            }



            return gameisended;
        }



}}