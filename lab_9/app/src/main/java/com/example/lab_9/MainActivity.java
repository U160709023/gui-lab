package com.example.lab_9;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

public class MainActivity extends Activity implements BoardView{
    BoardPresenter presenter;
    TableLayout boardView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        presenter = new BoardPresenter(this);
        boardView = findViewById(R.id.board);
//register button listeners
        for (byte row=0; row<3; row++){
            TableRow tableRow = (TableRow)boardView.getChildAt(row);
            for (byte col=0; col<3; col++){
                Button button = (Button)tableRow.getChildAt(col);
                BoardPresenter.CellClickListener clickListener = new
                        BoardPresenter.CellClickListener(presenter,row,col);
                button.setOnClickListener(clickListener);
                presenter.addCellClickListener(clickListener);
            }
        }
    }
    @Override
    public void newGame() {
        TableLayout boardView = findViewById(R.id.board);
        for (int row=0; row<3; row++){
            TableRow tableRow = (TableRow)boardView.getChildAt(row);
            for (int col=0; col<3; col++){
                Button button = (Button)tableRow.getChildAt(col);
                presenter.board.board[row][col] = 0;
                button.setText("");
                button.setEnabled(true);
            }
        }
    }
    @Override
    public void putSymbol(char symbol, byte row, byte col) {
        TableRow tableRow = (TableRow)boardView.getChildAt(row);
        Button button = (Button)tableRow.getChildAt(col);
        button.setText(Character.toString(symbol));
        int WhichPlayer = 0;
        if(isFull()){
            gameEnded((byte) WhichPlayer);
            newGame();
        }
        if(symbol == (char) 'X'){
            WhichPlayer = 1;
        }
        else if(symbol == (char) 'O'){
            WhichPlayer = 2;
        }
        if(GameEnded(row,col,(byte) WhichPlayer) ){
            gameEnded((byte) WhichPlayer);
            newGame();
        }
    }
    @Override
    public void gameEnded(byte winner) {


        switch (winner){
            case BoardView.DRAW :
                Toast.makeText(this,"Game is Draw", Toast.LENGTH_LONG).show();
                break;
            case BoardView.PLAYER_1_WINNER :
                Toast.makeText(this,"Player 1 Wins", Toast.LENGTH_LONG).show();
                break;
            case BoardView.PLAYER_2_WINNER :
                Toast.makeText(this,"Player 2 Wins", Toast.LENGTH_LONG).show();
                break;
        }
    }

    public boolean GameEnded(int r, int c,byte WhichPlayerValue){
        boolean gameisended = true;

        //row control

        for(int i = 0; i < 3; i++){
            if(presenter.board.board[r][i] != WhichPlayerValue){
                gameisended = false;

            }

        }

        //column control
        if(gameisended){
            return gameisended;
        }
        gameisended= true;
        for(int i = 0; i < 3; i++){

            if(presenter.board.board[i][c] != WhichPlayerValue){
                gameisended = false;

            }

        }

        //diagonal control

        if(gameisended){
            return gameisended;
        }
        gameisended= false;
        if((presenter.board.board[0][0] == WhichPlayerValue && presenter.board.board[1][1] == WhichPlayerValue&&
                presenter.board.board[2][2] == WhichPlayerValue) || (presenter.board.board[0][2] == WhichPlayerValue &&
                presenter.board.board[1][1] == WhichPlayerValue && presenter.board.board[2][0] == WhichPlayerValue)){
            gameisended = true;

        }



        return gameisended;
    }

public void sonucBastir(int winner){
    switch (winner){
        case BoardView.DRAW :
            Toast.makeText(this,"Game is Draw", Toast.LENGTH_LONG).show();
            break;
        case BoardView.PLAYER_1_WINNER :
            Toast.makeText(this,"Player 1 Wins", Toast.LENGTH_LONG).show();
            break;
        case BoardView.PLAYER_2_WINNER :
            Toast.makeText(this,"Player 2 Wins", Toast.LENGTH_LONG).show();
            break;
    }
}


    public boolean isFull(){

        boolean isFull = false;
        for(int i = 0; i < 3 ; i++){
            for(int j = 0; j < 3; j++){
                if(presenter.board.board[i][j] == 0) return false;
            }
        }
        return true;
    }

}
