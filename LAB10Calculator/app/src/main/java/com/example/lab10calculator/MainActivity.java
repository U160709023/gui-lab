package com.example.lab10calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TableRow;

public class MainActivity extends AppCompatActivity implements CalculatorView{
    Presenter presenter;
    EditText txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txt = findViewById(R.id.txtNum);

        presenter = new Presenter(this);


        TableLayout table = findViewById(R.id.numPad);
        for (int i=0; i< table.getChildCount(); i++){
            TableRow row = (TableRow)table.getChildAt(i);
            for (int j=0; j< row.getChildCount(); j++){
                View view = row.getChildAt(j);
                view.setOnClickListener(presenter);
            }
        }

    }




    public void setNumber(String result) {
        txt.setText(result);
    }
    @Override
    public String getNumber() {
        return txt.getText().toString();
    }
}
