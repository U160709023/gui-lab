package com.example.lab10calculator;

import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class Presenter implements View.OnClickListener, CalculatorListener{
    CalculatorView CalculatorView;
    boolean CanBeNumber = true;
    SimpleCalculator simpleCalculator;
     boolean CanBeOperator = false;
     boolean hasToBeNumber = true;


    List<Object> list = new ArrayList<>();


    public Presenter(CalculatorView CalculatorView){

        this.CalculatorView = CalculatorView;
        simpleCalculator = new SimpleCalculator(this);
    }


    @Override
    public void onClick(View v) {
        Button btn =(Button) v;
        String symbol = btn.getText().toString();

        if(symbol.equals("CLEAR")){
            setNumber("");
            for(int a = 0; a < list.size();a++){
                list = new ArrayList<>();}
        }

         else if(!symbol.equals("=") && !symbol.equals("CLEAR")){
            list.add(symbol);
        }
            if(!symbol.equals("CLEAR")) {
                CalculatorView.setNumber(CalculatorView.getNumber() + symbol);
            }
            if(symbol.equals("=")){
                System.out.println(list);
                System.out.println(""+simpleCalculator.TakeOperator(0,list));
                onResultCalculated(simpleCalculator.TakeOperator(0,list));
            }




    }

    @Override


    public void onResultCalculated(int result){
        setNumber(String.valueOf(result));
    }


    @Override
    public void setNumber(String result) {
        CalculatorView.setNumber(result);
    }
}
