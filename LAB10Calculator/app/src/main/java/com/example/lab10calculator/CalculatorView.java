package com.example.lab10calculator;

public interface CalculatorView {

    public void setNumber(String result);

    public String getNumber();
}
